﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestApp
{
    class CommandManager
    {
        //классическое написание
        //private static readonly object lockObj = new object();
        //private static CommandManager instance = null;
        //ленивое написание
        private static readonly Lazy<CommandManager> instance =
            new Lazy<CommandManager>(()=> new CommandManager());

        private LinkedList<ICommand> commands = new LinkedList<ICommand>();
        private LinkedListNode<ICommand> currentCommand;

        public static CommandManager Instance
        {
            get
            {
                return instance.Value;
            }
        }

        private CommandManager()
        {

        }

        

        public void ExecuteCommand(ICommand command)
        {
            if (currentCommand?.Next != null)
            {
                while (currentCommand != commands.Last)
                {
                    commands.RemoveLast();
                }
            }
            commands.AddLast(command);
            currentCommand = commands.Last;
            currentCommand.Value.Execute();
        }

        public void UnDoCommand()
        {
            if (currentCommand != null)
            {
                currentCommand.Value.UnDo();
                currentCommand = currentCommand.Previous;
            }
        }

        public void ReDoCommand()
        {
            if (currentCommand.Next != null)
            {
                currentCommand.Value.ReDo();
                currentCommand = currentCommand.Next;
            }
        }
    }
}
