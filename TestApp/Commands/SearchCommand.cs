﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.Commands
{
    class SearchCommand: ICommand
    {
        private SearchModel model;
        public SearchCommand(SearchModel model)
        {
            this.model = model;
        }

        public void Execute()
        {
            model.Search();
            model.GetResult();
        }

        public void UnDo()
        {
            throw new NotImplementedException();
        }

        public void ReDo()
        {
            throw new NotImplementedException();
        }
    }
}
