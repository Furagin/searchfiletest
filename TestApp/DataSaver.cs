﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class DataSaver
    {
        private static DataSaver instance;

        public string Name { get; private set; }
        private static object syncRoot = new Object();

        protected DataSaver(string name)
        {
            this.Name = name;
        }

        public static DataSaver GetInstance(string name)
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        instance = new DataSaver("Name");
                    }
                }
            }
            return instance;
        }
    }
}
