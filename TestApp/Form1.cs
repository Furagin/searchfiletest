﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestApp.Commands;

namespace TestApp
{
    public partial class Form1 : Form
    {
        private CommandManager managerCommand;
        private DataSaver dataSaver;
        private SearchModel model;

        private static ManualResetEvent mre = new ManualResetEvent(false);
        private Thread searchThread;

        public delegate void SetCurrentFileName(Label fileNameLabel, string text);
        public Form1()
        {
            InitializeComponent();
            //объект, управляющий выполнениями
            //managerCommand.;
            //объект, хронящий данные
            dataSaver = DataSaver.GetInstance("DataSaver");
            //поисковая модель
            model = new SearchModel(ref fileNameLabel);
        }

        private void InfoButton_Click(object sender, EventArgs e)
        {
            Form2 frmInfo = new Form2();
            frmInfo.Show();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {

        }
        //задание статовой директории
        private void SelectDirectoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog newDirectory = new FolderBrowserDialog();
            newDirectory.ShowDialog();
            string selectedPath = newDirectory.SelectedPath;
            model.SetDirectory(selectedPath);
            if (selectedPath != "") startDirectoryTextBox.Text = selectedPath;
        }
        //формирование начальной модели
        private void SetSearchCharacters()
        {
            try
            {
                model.SetDirectory(startDirectoryTextBox.Text);
                model.SetPattern(startPatternTextBox.Text);
                model.SetSample(startSampleTextBox.Text);
            }
            catch (Exception e)
            {
                Form2 frmInfo = new Form2("Problem with characters in form");
                frmInfo.Show();
            }
        }
    }
}
