﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            StartInfo();
        }

        public Form2(string problem)
        {
            InitializeComponent();
            infoTextBox.Text = problem;
        }

        private void StartInfo()
        {
            infoTextBox.Text = "" + Environment.NewLine;
        }
    }
}
