﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    interface ICommand
    {
        void Execute();
        void UnDo();
        void ReDo();
    }
}
