﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Threading;

namespace TestApp
{
    class SearchModel
    {
        private string researchDirectory;
        private string researchPattern;
        private string researchSample;

        private string[] arrayPath;
        private List<string> listFoundFile = new List<string>();

        private Label labelFileName;

        public SearchModel(ref Label fileNameLabel)
        {
            researchDirectory = System.IO.Directory.GetCurrentDirectory();
            researchPattern = "*.txt";
            researchSample = "";
            labelFileName = fileNameLabel;
        }

        public string GetDirectory()
        {
            return researchDirectory;
        }
        public void SetDirectory(string directory)
        {
            researchDirectory = directory;
        }

        public void SetPattern(string pattern)
        {
            researchPattern = pattern;
        }public string GetPattern()
        {
            return researchPattern;
        }

        public void SetSample(string sample)
        {
            researchSample = sample;
        }
        public string GetSample()
        {
            return researchSample;
        }

        public List<string> GetResult()
        {
            return listFoundFile;
        }

        public void Search()
        {
            try
            {
                arrayPath = Directory.GetFiles(researchDirectory, researchPattern);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }

            foreach (string currentFile in arrayPath)
            {
                SetLabelInForm(currentFile);
                if (SearchInFile(currentFile, researchSample))
                {
                    listFoundFile.Add(currentFile);
                }
            }
        }

        private bool SearchInFile(string pathToFile, string sample)
        {
            bool resultResearch = false;
            //
            string textCurrentFile = File.ReadAllText(pathToFile);
            if (textCurrentFile.IndexOf(sample) >= 0)
            {
                resultResearch = true;
            }
            //
            return resultResearch;
        }
        private void SetLabelInForm(string fileName)
        {
            labelFileName.Invoke(new Action(() => { labelFileName.Text = fileName; }));
            //Задержка проверки (затем удалиться)
            Thread.Sleep(1000);
        }
    }
}
